package com.aeditip.introspringbcd.wc2018.controladores;

/**
 * Clase que define una estereotipo de controlador estándar de Spring para la clase Persona.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.3
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.introspringbcd.wc2018.entidades.Jugador;
import com.aeditip.introspringbcd.wc2018.entidades.Persona;
import com.aeditip.introspringbcd.wc2018.servicios.ServicioPersona;

// Anotación para anotar esta clase como un controlador estereotipado de Spring para la entidad Riesgo.
@RestController
// Ruta general de la ruta de los métodos que controla este objeto.
@RequestMapping("/api")
//Solución de mecanismo CORS.
@CrossOrigin(origins="*", methods = {RequestMethod.GET})
public class ControladorPersona {
	// Declaración de servicio a utilizar.
	@Autowired
	private ServicioPersona servicio;
	
	/* Solicita al servicio relacionado obtener una persona de la base de datos, dado un identificador
	 * 	determinado.
	 * @param	id	Identificador de la persona registrado en la base de datos. 
	 * @return		Un objeto de la clase Persona con los datos registrados en la base de datos.
	 */
	@RequestMapping(value = "personas/{id}", method = RequestMethod.GET)
	public Persona obtenerPersonaEspecifica(@PathVariable int id){
		return servicio.obtenerPersonaEspecifica(id);
	}
	
	/* Solicita al servicio relacionado obtener un jugador de la base de datos, dado un identificador
	 * 	determinado.
	 * @param	id	Identificador del jugador registrado en la base de datos. 
	 * @return		Un objeto de la clase Jugador con los datos registrados en la base de datos.
	 */
	@RequestMapping(value = "jugadores/{id}", method = RequestMethod.GET)
	public Jugador obtenerJugadorEspecifica(@PathVariable int id){
		return servicio.obtenerJugadorEspecifica(id);
	}
}
