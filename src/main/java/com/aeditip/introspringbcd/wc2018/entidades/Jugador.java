package com.aeditip.introspringbcd.wc2018.entidades;

/**
 * Clase que define la entidad para la tabla Jugador.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// Anotación de Lombok que indica que los atributos de la clase padre estarán involucrados o no
//	en las funcionales .equals() y .hashCode(), a través del parámetro "callSuper".
@EqualsAndHashCode(callSuper=false)
// Especificación de entidad
@Entity
// La estrategia JOINED requiere esta anotación para la herencia. Debe colocar el nombre del
//	parámetro que representa a la llave primaria de su superclase.
@PrimaryKeyJoinColumn(referencedColumnName="id")
// Relación con la Tabla "jugador" en la base de datos.
@Table(name = "jugador")
// Esta clase es una subclase de la clase Persona, por lo que debemos extenderla.
public class Jugador extends Persona {
	@Column(name = "dorsal")
	private int dorsal;
	
	@Column(name = "posicion")
	private String posicion;
}
