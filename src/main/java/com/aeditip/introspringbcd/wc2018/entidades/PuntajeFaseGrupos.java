package com.aeditip.introspringbcd.wc2018.entidades;

/**
 * Clase que determina estadísticas de un país, dada una serie de partidos disputados por la
 * 	selección de este.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 *   1.2 (28/03/2021): Modificación de métodos tras la modificación del modelo de base de datos.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.2
 */

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PuntajeFaseGrupos {
	public Pais pais;
	public int puntos;
	public int fGoles;
	public int dGoles;
	
	/* Constructor de la clase, usando como parámetros el país y la lista de partidos disputados por este.
	 * @param	pais		Objeto del país en referencia.
	 * @param	partidos	Lista de partidos disputados por este país. Recuerde que Ud. manda la lista de partidos
	 * 						así que dependerá del contexto si la lista corresponde sólo a los partidos de Fase de 
	 * 						Grupos a todos los partidos que disputó el país, entre otros escenarios.
	 */
	public PuntajeFaseGrupos(Pais pais, List<Partido> partidos) {
		this.pais = pais;
		// Procesar datos de estadísticas
		// Se usa .stream() para convertir la lista (List) en Stream, para usar las funciones siguientes
		this.puntos = partidos.stream()
				// Se usa .filter(obj -> boolean) para solo quedarnos con los elementos que necesitamos
				// En este caso, nos quedaremos con los partidos donde el equipo que evaluamos fue local.
				.filter(x -> pais.getCodigo() == x.getLocal().getPais().getCodigo())
				// Se usa .mapToInt(obj -> Integer) para separar un atributo en particular.
				// La diferencia con map(obj -> obj) es el dato que debe separar (en .map() puede filtrarse cualquier objeto,
				//	en .mapToInt(), se necesita un Integer)
				// En este caso, calcularemos los puntos obtenidos según el resultado del partido (cuando gana obtiene
				//	tres puntos, cuando empata, sólo uno.
				.mapToInt(y -> (y.determinarDiferenciaGoles() > 0 ? 3 : (y.determinarDiferenciaGoles() == 0 ? 1 : 0)))
				// Por último, usaremos .sum() para realizar la suma de los valores enteros. Recuerde que .sum() solo
				//	puede realizarse después de .mapToInt(), así como otras funciones, como por ejemplo, .average().
				//	Recomiendo revisar la documentación correspondiente para más información
				.sum() +
				// Se repetirá el procedimiento anterior para los partidos donde el país fue visitante.
				partidos.stream().filter(x -> pais.getCodigo() == x.getVisitante().getPais().getCodigo())
				.mapToInt(y -> (y.determinarDiferenciaGoles() < 0 ? 3 : (y.determinarDiferenciaGoles() == 0 ? 1 : 0)))
				.sum();
		// Se usan las funciones anteriores para calcular los goles a foavor (recoerdar que tenemos este dato en goles
		// del local (en caso el país evaluado sea local) o en goles de visitante (en el caso inverso)
		// Goles de local
		this.fGoles = partidos.stream().filter(x -> pais.getCodigo() == x.getLocal().getPais().getCodigo())
				.mapToInt(y -> y.getLocal().getGoles()).sum() +
				// Goles de visitante
				partidos.stream().filter(x -> pais.getCodigo() == x.getVisitante().getPais().getCodigo())
				.mapToInt(y -> y.getVisitante().getGoles()).sum();
		// El mismo procedimiento para la diferencia de goles. Esto se puede calcular gracias al método de nuestra clase
		//	Partido determinarDiferenciaGoles()
		// Diferencia de goles de local (se suman los valores originales dado que nos interesan los goles de local)
		this.dGoles = partidos.stream().filter(x -> pais.getCodigo() == x.getLocal().getPais().getCodigo())
				.mapToInt(y -> y.determinarDiferenciaGoles()).sum() +
				// Diferencia de goles de visitante (se suman los valores invertidos dado que nos interesan los goles de
				// 	visitante. Recuerda que la función citada resta los goles del local con los del visitante; para el caso
				//	del visitante, esta operación se invertiría, ¿verdad?
				partidos.stream().filter(x -> pais.getCodigo() == x.getVisitante().getPais().getCodigo())
				.mapToInt(y -> -y.determinarDiferenciaGoles()).sum();
	}
	
	public int getCodigoPais() {
		return -((this.pais.getCodigo().charAt(0)-65)*27*27 + (this.pais.getCodigo().charAt(1)-65)*27 +
				(this.pais.getCodigo().charAt(2)-65));
	}
}
