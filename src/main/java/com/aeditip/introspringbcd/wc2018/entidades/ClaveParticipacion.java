package com.aeditip.introspringbcd.wc2018.entidades;

/**
 * Clase que define la clave primaria de la tabla Participacion.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// Anotación que indica que una clase será embebida por otras.
@Embeddable
// El método embebido fuerza a que las claves se declaren como Serializables.
// La siguiente línea permite suprimir la advertencia de declarar un número serial a una clase serializable.
@SuppressWarnings("serial")
public class ClaveParticipacion implements Serializable {
	@Column(name="id_pais")
	String idPais;
	
	@Column(name="id_edicion")
	int idEdicion;
}
