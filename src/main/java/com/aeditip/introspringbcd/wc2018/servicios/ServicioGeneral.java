package com.aeditip.introspringbcd.wc2018.servicios;

/**
 * Clase que define una estereotipo de servicio estándar de Spring para propósitos generales.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 *   1.2 (27/03/2021): Modificación de métodos según las modificaciones para la Sesión 2.
 *   1.3 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.3
 */

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.aeditip.introspringbcd.wc2018.entidades.Edicion;
import com.aeditip.introspringbcd.wc2018.entidades.FasePartido;
import com.aeditip.introspringbcd.wc2018.entidades.Pais;
import com.aeditip.introspringbcd.wc2018.entidades.Partido;
import com.aeditip.introspringbcd.wc2018.entidades.PuntajeFaseGrupos;
import com.aeditip.introspringbcd.wc2018.entidades.partido.Organizacion;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioEdicion;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioFasePartido;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioPais;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioPartido;

// Anotación para anotar esta clase como un servicio estereotipado de Spring para propósitos generales.
@Service
public class ServicioGeneral {
	// Declaración de repositorios a utilizar.
	@Autowired
	private RepositorioEdicion repoEdicion;
	@Autowired
	private RepositorioPartido repoPartido;
	@Autowired
	private RepositorioFasePartido repoFase;
	@Autowired
	private RepositorioPais repoPais;
	
	/* Determina los dos primeros puestos de un determinado grupo (compuesto por cuatro países). Se usaron funciones
	 * 	de la clase Stream para reducir el código empleado, dado que el procesamiento, ordenamiento y selección de datos
	 * 	ocuparían variables locales y líneas de código adicionales, además de la adición de estructuras iterativas.
	 * @param	organizacion	Datos de organización de un partido.
	 * @param	paises			Lista de países.
	 * @return					Una lista de dos países: la posición 0 corresponde al primer puesto, la posición 1, al segundo.
	 */
	private List<Pais> obtenerDosMejores(Organizacion organizacion, List<Pais> paises) {
		// A través de la función map se crean objetos de la clase PuntajeFaseGrupos para obtener las estadísticas por
		//	país (la clase mencionada realiza el procesamiento y los cálculos requeridos).
		List<PuntajeFaseGrupos> estadisticas = paises.stream()
				.map(x -> new PuntajeFaseGrupos(x, repoPartido.obtenerPartidosdePaisEspecifico(organizacion.getEdicion().getAnho(), x.getCodigo())))
				.collect(Collectors.toList());
		// Ordenar según los criterios establecidos.
		// Ordena por puntaje.
		estadisticas.sort(Comparator.comparingInt(PuntajeFaseGrupos::getPuntos)
				// Ordena por diferencia de goles.
				.thenComparingInt(PuntajeFaseGrupos::getDGoles)
				// Ordena por goles a favor.
				.thenComparingInt(PuntajeFaseGrupos::getFGoles)
				// Ordena por orden alfabético del código del país.
				.thenComparingInt(PuntajeFaseGrupos::getCodigoPais).reversed());
		// Impresión de las tablas de posiciones de los ocho grupos (para comprobar el funcionamiento de lo implementado)
		// Primer puesto (clasificado a Octavos de Final)
		System.out.println("Primer puesto (C): Pais: " + estadisticas.get(0).pais.getCodigo() + " Ptos. " +
				estadisticas.get(0).puntos + " GF: " + estadisticas.get(0).fGoles + " DG: " + estadisticas.get(0).dGoles);
		// Segundo puesto (clasificado a Octavos de Final)
		System.out.println("Segundo puesto (C): Pais: " + estadisticas.get(1).pais.getCodigo() + " Ptos. "
				+ estadisticas.get(1).puntos + " GF: " + estadisticas.get(1).fGoles + " DG: " + estadisticas.get(1).dGoles);
		// Tercer puesto.
		System.out.println("Tercer puesto: Pais: " + estadisticas.get(2).pais.getCodigo() + " Ptos. " +
				estadisticas.get(2).puntos + " GF: " + estadisticas.get(2).fGoles + " DG: " + estadisticas.get(2).dGoles);
		// Cuarto puesto.
		System.out.println("Cuarto puesto: Pais: " + estadisticas.get(3).pais.getCodigo() + " Ptos. " +
				estadisticas.get(3).puntos + " GF: " + estadisticas.get(3).fGoles + " DG: " + estadisticas.get(3).dGoles);
		System.out.println("");
		// Retorna los dos primeros puestos (los países que están en las posiciones 0 y 1 de la lista)
		return estadisticas.subList(0, 2).stream().map(x -> x.getPais()).collect(Collectors.toList());
	}
	
	/* Determina los dos primeros puestos de un determinado grupo (compuesto por cuatro países). Se usaron funciones
	 * 	lambda para reducir el código empleado, dado que el procesamiento, ordenamiento y selección de datos ocuparían
	 * 	variables locales y líneas de código adicionales, además de la adición de estructuras iterativas.
	 * @param	organizacion	Datos de organización de un partido.
	 * @param	grupoX			Primer grupo del par (denominado "Grupo X").
	 * @param	grupoY			Segundo grupo del par (denominado "Grupo Y").
	 * @return					Una lista de dos partidos de octavos de final generados para los dos puestos de cada par de grupos.
	 */
	private List<Partido> crearPartidosOctavosporGrupo(Organizacion organizacion, String grupoX, String grupoY) {
		// Crear una lista de partidos, originalmente vacía.
		List<Partido> auxParPartidos = new ArrayList<Partido>();
		// Obtener dos mejores del primer grupo.
		List<Pais> mejoresX = obtenerDosMejores(organizacion,
				repoPais.obtenerPaisesdeGrupoEspecifico(organizacion.getEdicion().getAnho(), grupoX));
		// Obtener dos mejores del segundo grupo.
		List<Pais> mejoresY = obtenerDosMejores(organizacion,
				repoPais.obtenerPaisesdeGrupoEspecifico(organizacion.getEdicion().getAnho(), grupoY));		
		// Agrega el primer partido: 1° X v. 2° Y.
		auxParPartidos.add(new Partido(organizacion, mejoresX.get(0), mejoresY.get(1)));		
		// Agrega el segundo partido: 1° Y v. 2° X.
		auxParPartidos.add(new Partido(organizacion, mejoresY.get(0), mejoresX.get(1)));
		// Devuelve los dos partidos creados.
		return auxParPartidos;
	}
	
	/* Imprime un mensaje de bienvenida.
	 */
	public String bienvenida() {
		return "Hola Mundo! La sesión 3 está genial :D Próximamente taller de python";
	}
	
	/* Genera los partidos de Octavos de Final de la Copa.
	 * @param	edicion		Año de la edición de la Copa Mundial.
	 */
	public void finalizarFaseGrupos(@RequestParam int edicion) {
		// Obtiene el objeto representativo de la organización del partido correspondiente a los Octavos de Final.
		FasePartido fase = repoFase.findById(2).get();
		Edicion copa = repoEdicion.findById(edicion).get();
		Organizacion organizacion = new Organizacion(copa, fase);
		// Lista de par de partidos de cada par de grupos.
		List<Partido> auxParPartidos = new ArrayList<Partido>();
		// Lista del primer sector de partidos (1° del Grupo X vs. 2° del Grupo Y)
		List<Partido> aux1X2Y = new ArrayList<Partido>();
		// Lista del segundo sector de partidos (1° del Grupo Y vs. 2° del Grupo X)
		List<Partido> aux1Y2X = new ArrayList<Partido>();
		// Generación de los partidos de los Grupos A y B.
		auxParPartidos = crearPartidosOctavosporGrupo(organizacion, "A", "B");
		aux1X2Y.add(auxParPartidos.get(0));
		aux1Y2X.add(auxParPartidos.get(1));
		// Generación de los partidos de los Grupos C y D.
		auxParPartidos = crearPartidosOctavosporGrupo(organizacion, "C", "D");
		aux1X2Y.add(auxParPartidos.get(0));
		aux1Y2X.add(auxParPartidos.get(1));
		// Generación de los partidos de los Grupos E y F.
		auxParPartidos = crearPartidosOctavosporGrupo(organizacion, "E", "F");
		aux1X2Y.add(auxParPartidos.get(0));
		aux1Y2X.add(auxParPartidos.get(1));
		// Generación de los partidos de los Grupos G y H.
		auxParPartidos = crearPartidosOctavosporGrupo(organizacion, "G", "H");
		aux1X2Y.add(auxParPartidos.get(0));
		aux1Y2X.add(auxParPartidos.get(1));
		// Registrar todos los partidos en orden. Recuerde que .saveAll() registra todos los elementos de una lista.
		repoPartido.saveAll(aux1X2Y);
		repoPartido.saveAll(aux1Y2X);
	}
	
	/* Genera los partidos de Cuartos de Final de la Copa.
	 * @param	edicion		Año de la edición de la Copa Mundial.
	 */
	public void finalizarOctavosdeFinal(@RequestParam int edicion) {
		// Estructuración de la organización del partido.
		FasePartido fase = repoFase.findById(3).get();
		Edicion copa = repoEdicion.findById(edicion).get();
		Organizacion organizacion = new Organizacion(copa, fase);
		// Llama a la función de repositorio para los partidos de Octavos de Final.
		List<Partido> partidos =  repoPartido.obtenerPartidosdeFaseEspecifica(copa.getAnho(), 2);
		// Creación de partidos de cuartos de final.
		repoPartido.save(new Partido(organizacion, partidos.get(0).obtenerGanador(), partidos.get(1).obtenerGanador()));
		repoPartido.save(new Partido(organizacion, partidos.get(2).obtenerGanador(), partidos.get(3).obtenerGanador()));
		repoPartido.save(new Partido(organizacion, partidos.get(4).obtenerGanador(), partidos.get(5).obtenerGanador()));
		repoPartido.save(new Partido(organizacion, partidos.get(6).obtenerGanador(), partidos.get(7).obtenerGanador()));
	}
}
