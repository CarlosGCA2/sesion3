package com.aeditip.introspringbcd.wc2018.repositorios;

/**
 * Clase que define una estereotipo de repositorio estándar de Spring para la clase Pais.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 *   1.2 (27/03/2021): Modificación de métodos por el cambio de modelo de base de datos.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.2
 */

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aeditip.introspringbcd.wc2018.entidades.Pais;

// Anotación para anotar esta clase como un repositorio estereotipado de Spring para la entidad País.
@Repository
public interface RepositorioPais extends JpaRepository<Pais, String> {
	/* Solicita a la base de datos los países que pertenecen a un grupo específico, en una edición específica.
	 * @param	idEdicion	Identificación de la edición de la Copa Mundial en consulta.
	 * @param	grupo		Grupo en consulta.
	 * @return				Un lista de Paises que pertenecen a un grupo específico, en una edición específica.
	 */
	@Query(value = "SELECT a.* FROM pais a INNER JOIN participacion b ON b.id_pais = a.codigo WHERE b.id_edicion = ?1 AND b.grupo = ?2",
			nativeQuery = true)
	List<Pais> obtenerPaisesdeGrupoEspecifico(int idEdicion, String grupo);
}
