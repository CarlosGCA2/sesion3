package com.aeditip.introspringbcd.wc2018.repositorios;

/**
 * Clase que define una estereotipo de repositorio estándar de Spring para la clase Partido.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 *   1.2 (27/03/2021): Modificación de métodos por el cambio de modelo de base de datos.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.2
 */

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aeditip.introspringbcd.wc2018.entidades.Partido;

// Anotación para anotar esta clase como un repositorio estereotipado de Spring para la entidad Partido.
@Repository
public interface RepositorioPartido extends JpaRepository<Partido, Integer> {
	/* Solicita a la base de datos los partidos disputados por un país específico, tanto de local como
	 * 	de visitante, en una edición específica.
	 * @param	idEdicion	Identificación de la edición de la Copa Mundial en consulta.
	 * @param	idPais		Identificador del país. 
	 * @return				Un lista de Partidos disputados por un país específico, tanto de local como
	 * 						de visitante, en una edición específica.
	 */
	@Query(value = "SELECT * FROM partido WHERE id_edicion = ?1 AND (id_local = ?2 OR id_visitante = ?2)", nativeQuery = true)
	List<Partido> obtenerPartidosdePaisEspecifico(int idEdicion, String idPais);
	
	/* Solicita a la base de datos los partidos de una fase específica, en una edición específica.
	 * @param	idEdicion	Identificación de la edición de la Copa Mundial en consulta.
	 * @param	idPais		Identificador del país. 
	 * @return				Un lista de Partidos de una fase específica, en una edición específica.
	 */
	@Query(value = "SELECT * FROM partido WHERE id_edicion = ?1 AND id_fase = ?2 ORDER BY ID", nativeQuery = true)
	List<Partido> obtenerPartidosdeFaseEspecifica(int idEdicion, int idFase);
}
