# Taller de Introducción a Spring Boot y Despliegue Continuo 2021

¡Bienvenido al Repositorio del Taller de Introducción a Spring Boot y Despliegue Continuo 2021! Aquí se alojan los archivos del proyecto de las sesiones del Taller. Este proyecto resuelve el Problema de Aplicación propuesto para el Taller, tanto en sus versiones básica como sus variables. De parte del Grupo AEDITIP y de la Academia Temple, agradecemos su visita a este repositorio y esperamos que el contenido de este.

# Tabla de Contenido
1. [Sobre el Taller](#sobre)
2. [Docentes](#docentes)
    1. [Historia](#historia)
3. [Ramas del Repositorio](#ramas)
4. [Indicaciones](#indicaciones)
5. [Consultas](#consultas)
6. [Agradecimientos especiales](#gracias)
7. [Contacto](#contacto)

## Sobre el Taller <a name="sobre"></a>

El Taller de Introducción a Spring Boot y Despliegue Continuo 2021 es organizado por el [Grupo AEDITIP](https://www.linkedin.com/company/aeditip/) y la [Academia Temple](https://academiatemple.com/). El taller consta de tres (03) sesiones, las cuales fueron realizadas del 20 de marzo al 10 de abril de 2021. Para más información, puede revisar el temario oficial del Taller a través de }[este enlace](https://drive.google.com/file/d/1vQyo-7BWYeTnlf-Gsh6ZgFarBu9lx_-u/view?usp=sharing).

### Historia <a name="historia"></a>

Originalmente, este Taller fue organizado por el Grupo AEDITIP con la colaboración de la organización de apoyo a emprendedores [E-quipu](https://www.equipu.pe/) y estuvo planificado para realizarse entre el 10 y el 14 de marzo de 2020. Este Taller significó el primer evento académico tras el cierre temporal del Grupo AEDITIP, antes llamada la Agrupación Estudiantil de Apoyo en Informática (AEAI), tras una serie de señalamientos injustificados por parte de autoridades en una especialidad de la Pontificia Universidad Católica del Perú (PUCP), y el primero tras el cambio de nombre definitivo.

La primera sesión se desarrolló el día martes, 10 de marzo de 2020, en un aula del Pabellón Z de la Pontificia Universidad Católica del Perú. Sin embargo, el Estado Peruano, frente a la avanzada de la pandemia originada por la expansión del COVID-19 en territorio nacional, decretó un Estado de Emergencia Sanitaria para evitar la propagación del mencionado virus. Ese mismo día, la Pontificia Universidad Católica del Perú estableció que todas las actividades presenciales programadas en su campus sean reprogramadas hasta el fin de las medidas de cuarentena (inicialmente programadas para el día lunes, 30 de marzo de 2020). Ante esto, el Grupo AEDITIP emitió el día siguiente el [Comunicado N° 001-2020/AEDITIP](https://drive.google.com/file/d/1Rx2hrr1aoHLs484wqwVyv5Ac69j5bxZP/view?usp=sharing)

Las medidas de confinamiento y cuarentena iniciadas en marzo se prolongaron indefinidamente, siendo imposible la reprogramación del Taller de manera presencial. Ante esto, el Grupo AEDITIP emitió el [Comunicado N° 002-2020/AEDITIP](https://drive.google.com/file/d/1n39vF9lKdEHf3cX8fAj8WxC6FB4hNre4/view?usp=sharing), indicando la virtualización del evento. Sin embargo, el Grupo AEDITIP entró en una pausa de actividades para brindar a sus integrantes el tiempo que necesitaran para terminar sus proyectos de tesis.

A inicios de marzo de 2021, el Grupo AEDITIP y la Academia Temple establecieron una alianza estratégica para la realización de eventos académicos en conjunto. Como primer Taller de esta alianza, se decidió reorganizar el Taller de marzo de 2020, con una reestructuración del temario propuesto.

## Docentes <a name="docentes"></a>

- [B.Sc. Jorge Fatama Vera](https://www.linkedin.com/in/jfatamav/): Egresado de la Pontificia Universidad Católica del Perú (PUCP). Co-fundador y Director del Grupo AEDITIP. Docente de las sesiones 1 y 2.
- [Sr. Carlos Carrillo Agoyo](https://www.linkedin.com/in/carlos-carrillo-agoyo-a3b7a0129/): Egresado de la Pontificia Universidad Católica del Perú (PUCP). Co-fundador del Grupo AEDITIP. ITOps Engineer en everis. Profesional certificado en Amazon Web Services (AWS) y Azure. Docente de la tercera sesión.

## Ramas del Repositorio <a name="ramas"></a>

|  Nombre | Descripción | Disponible a partir de |
| ------- | ----------- | ---------------------- |
| master | Esta rama | 2021/03/22 |
| sesion1 | Proyecto generado en la primera sesión | 2021/03/22 |
| sesion2 | Proyecto generado en la segunda sesión | 2021/03/28 |
| sesion3 | Proyecto generado en la tercera sesión | 2021/04/11 |

## Indicaciones <a name="indicaciones"></a>

- Cada sesión tendrá una rama de desarrollo. En particular, cada rama tendrá su propio archivo descritor (README.md), en el cual se mostrarán indicaciones y descripciones que debe tomar en cuenta antes de descargar el contenido de la rama.
- Es probable que, en base a las sugerencias obtenidas en nuestros ciclos de mejora continua, se realicen actualizaciones, por lo que el contenido del repositorio está sujeto a cambios. Puede ver el historial de modificaciones en el detalle de la rama.
- Puede revisar las sesiones de clase en la página de nuestro aliado estratégico "Academia Temple", precisamente en [la página del Taller](https://academiatemple.com/course/?id=yPQWte2eFM8nyzpojLNW).
- Puede revisar [la carpeta del Taller en Google Drive](https://bit.ly/3lt4te5) para revisar los documentos oficiales del Taller. Al igual que las ramas de este repositorio, los documentos allí alojados están sujetos a modificaciones.

## Consultas <a name="consultas"></a>

Si tiene alguna duda, sugerencia o interrogante, no dude en realizarla a los correos [jorge.fatama@pucp.edu.pe](mailto:jorge.fatama@pucp.edu.pe?subject=[intro.springbcd.21]%20Consulta) o [carlosg.carrillo@pucp.edu.pe](mailto:carlosg.carrillo@pucp.edu.pe?subject=[intro.springbcd.21]%20Consulta). Cualquier sugerencia o consulta, brindada de manera respetuosa, será bienvenida y será tomada en cuenta para los ciclos de mejora continua que ejecutamos constantemente; desde ya, brindamos un agradecimiento de antemano por su colaboración.

## Agradecimientos especiales <a name="gracias"></a>

Un agradecimiento especial al [Sr. André Corrales Estrada](https://www.linkedin.com/in/robertandrecorralesestrada/), Vicepresidente de la Sociedad Estudiantil de Apoyo en Informática (SAI PUCP) por su colaboración en la elaboración del Texto oficial del Taller. Asimismo, un agradecimiento a la [Srta. Lis Paredes Pardo](https://www.linkedin.com/in/lis-paredes-pardo-ba6b75145/), Presidenta de SAI PUCP, por el arduo trabajo realizado para la realización del Taller original, que, aunque no pudo completarse, debe reconocerse. Finalmente, un agradecimiento a cada uno de los alumnos asistentes y a los alumnos que propusieron ideas y sugerencias para la mejora.

## Contacto <a name="contacto"></a>

Por favor, sigan nuestras redes sociales para estar al tanto de nuestros Talleres futuros y novedades (haga clic en los enlaces de la lista siguiente para acceder).
- [Página de Facebook.](https://www.facebook.com/aeditip)
- [Perfil de LinkedIn.](https://www.linkedin.com/company/aeditip/)

Asimismo, recomendamos seguir las redes sociales de nuestro aliado estratégico, Academia Temple.
- [Página de Facebook.](https://www.facebook.com/templealumnos/)
- [Página web.](https://academiatemple.com/)
